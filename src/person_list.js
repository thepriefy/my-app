import React from 'react';
import './App.css';

const PersonList = (props) => {
    return (
        <div className="person" onClick={props.clicked}>
            <p>Hello {props.name} ({props.age} years)</p>
        </div>
    )
}

export default PersonList