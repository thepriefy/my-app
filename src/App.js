import React, { Component } from 'react';
import './App.css';
import PersonList from './person_list'

class App extends Component {
    state = {
        people: [
            { id: 1, name: 'Mr.1', age: 24},
            { id: 2, name: 'Mr.2', age: 25},
            { id: 3, name: 'Mr.3', age: 26},
            { id: 4, name: 'Mr.4', age: 27},
        ]
    }

  render() {

    const onclickHandler = (id) => {
        const newPersons = [...this.state.people]

        /* using findIndex */
        // const personIndex = this.state.people.findIndex( person => { return id === person.id} )
        //
        // newPersons.splice( personIndex, 1)
        // this.setState({people: newPersons})

        /* using filter */
        this.setState({people: newPersons.filter( person => person.id !== id )})
    }

    const Person = this.state.people.map(person => {
        return <PersonList
            key={person.id}
            name={person.name}
            age={person.age}
            clicked={() => onclickHandler(person.id) } // this.[function name] ใช้เมื่อฟังก์ชันประกาศอยู่ในระดับเดียวกันกับ state
        />
    })

    return (
      <div>
          {Person}
      </div>
    );
  }
}

export default App;
